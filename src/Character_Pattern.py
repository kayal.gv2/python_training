# Python 3,x code to demonstrate star pattern

# Function to demonstrate
def alphapat(n):

    # initializing value corresponding to 'A'
    # ASCII value
    num = 65

    # outer loop to handle of rows
    # 5 in this case
    for i in range(0, n):

        # inner loop to handle numner of columns
        # values changing acc. to outer loop
        for j in range(0, i+1):

           # explicity converting  to char
           ch = chr(num)

           #printing char value
           print(ch, end=" ")
        # incrementing number
        num = num + 1

        #ending line after each row
        print("\r")
# Driver code
n = 5
alphapat(n)
