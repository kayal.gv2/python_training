# Python code 3,x to demonstrate star pattern

# Function to demonstrate printing pattern of alphabets
def contalpha(n):
    # initializing value corresponding to 'A'
    # ASCII values
    num = 65
    # outer loop to handle numer of columns
    for i in range(0, n):

        # inner lop to handle number of columns
        # values changing acc. to outer loop
        for j in range (0, i+1):

            # expllicity converting to char
            ch = chr(num)

            # printing char value
            print(ch, end=" ")

            # incrementing at each collumn
            num = num +1

        # ending line after each row
        print("\r")

# driver code
n = 5
contalpha(n)
