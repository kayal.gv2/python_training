# Demo program for seeek and tell
# Creating a file
str = "Python programming is fun"
fo = open ("filehandling.txt","w")
fo.write(str)
fo.close()
fo = open("filehandling.txt","r+")
s = fo.read();
print("Read string is :",s)
# check current position
pos = fo.tell();
print("Current file position :", pos)
# Reposition  pointer at the beginning once again
pos = fo .seek (0,0);
s = fo .read (10);
print("Again read string is :",s)
#close opened file
fo.close()
