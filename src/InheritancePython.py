# parent class
class Person:
    def __init__(self, fname, lname):
        self.firstname = fname
        self.lastname = lname


    def printname(self):
      print(self.firstname, self.lastname)
# Use the Person clss to create an object, and then execute the printname method:

# child class

class Student(Person):
    pass

x = Student("John", "Doe")
x.printname()
