""" Mad Libs Generators
-----------------------
"""
#// Loop back to this point once code finishes
loop = 1

while (loop < 10):
# All the questions that the program asks the user

    noun = input(" Choose a noun:")

    p_noun = input("Choose a plural noun:")

    noun2 = input("Choose a noun:")

    place = input("Name a place:")

    adjective = input("Choose an adjective (Deserving word): ")

    noun3 =  input("Choose a noun: ")
# Displays the story based on the users input
print("-----------------------")

print("Be kind to your", noun,"- footed", p_noun)

print("For a duck may be somebody's", noun2)

print("Be kind your", p_noun, "in", place )

print(" Where the weather is always", adjective, ".")

print()

print("You may think is this the", noun3, ",")

print("Well it is.")

print("----------------------")

# Loop back to "loop= 1"
loop = loop + 1
