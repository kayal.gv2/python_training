#Write a python function that checks whether a passed string is palindrome or not.import the module to find whether a string is palindrome.
from PalindromCheck import isPalindrome
str1 = input("Enter a String:")
f = isPalindrome(str1)
if f :
    print("The string ",str1," is a palindrome")
else:
    print("The string",str1,"is not a palindrome")
