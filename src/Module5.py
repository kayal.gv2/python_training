#Write a python function that prints out first n rows of the Pascal;s triangle .Import the module and accept n from the user.
#import Pascal.py
def pascal_triangle(n):
    trow = [1]
    y = [0]
    for x in range(max(n,0)):
        print(trow)
        trow = [1+r for l,r in zip(trow+y,y+trow)]
    return n>=1
#
import Pascal as p
n = int(input("Enter a limit:"))
p.pascal_triangle(n)
