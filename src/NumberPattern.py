# Python 3.x code to demonstrate star pattern

# Function to demonstrate printing pattern of numbers
def numpat(n):

    #initialising strarting numbers
    num = 1

    # outer loop to hamdle number of rows
    for i in range(0, n):
        num = 1
        #inner loop to handle number of columns
        # values changing acc. to outer loop
        for j in range(0, i+1):
         # printing numbers
            print(num, end=" ")

             # incrementing number at each columns
            num  = num + 1

          # ending line after each rows
        print("\r")
# driver code
n = 5
numpat(n)
