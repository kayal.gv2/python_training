#Write a Python function to create  and print a list where the values are square of numbers between two limits (both included ).Import the package to implement this.
def printValues(a,b):
    l = list()
    for i in range(a,b+1):
        l.append(i**2)
    print(l)
        
