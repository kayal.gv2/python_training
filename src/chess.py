#movement of Bishop in Chess
r  = int( input("Enter row number:"))
c  = int(input("Enter column number:"))
while(c in range(1,9) and r in range(1,9)):
    print("Possible Movements in (row,col)")
    if r==1 and c==1:
        print(r,c+1)
        print(r+1,c)
        break
    elif r==1 and c==8:
         print(r,c-1)
         print(r+1,c)
         break
    elif c==1 and r==8:
        print(r-1,c)
        print(r,c+1)
        break
    elif c==8 and r==8:
        print(r-1,c)
        print(r,c-1)
        break
    elif c==1 and r<8:
        print(r,c+1)
        print(r+1,c)
        print(r-1,c)
        break
    elif r==1:
        print(r+1,c)
        print(r,c+1)
        print(r,c-1)
        break
    elif c==8:
        print(r-1,c)
        print(r,c+1)
        print(r,c-1)
        break
    elif r==8:
        print(r-1,c)
        print(r,c+1)
        print(r,c-1)
        break
    else:
        print(r,c-1)
        print(r,c+1)
        print(r+1,c)
        print(r-1,c)
        break
else:
    print("Invalid range for row or column")                                    
