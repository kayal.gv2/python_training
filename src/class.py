class student:
    "common base class for all students"
    def __init__(self,rollno,name,course):
        self.rollno=rollno
        self.name=name
        self.course=course
    def displaystudent(self):
        print("Roll Number:",self.rollno)
        print("Name:",self.name)
        print("Course:",self.course)
stud1= student(1,'ggg','uuuu')
print(stud1)
stud1.displaystudent()
#built n attribute
class student:
    "student information"
    def __init__(self,regno,name,dept):
        self.regno=regno
        self.name=name
        self.dept=dept
    def detailstudent(self):
        print(" Regno:",self.regno)
        print("Name:",self.name)
        print("Dept:",self.dept)
stud2= student(32,'reegan','biology')
print(stud2)
stud2.detailstudent()
at=getattr(stud2,'name')
print("getattr(stud2,'name'):",at)
print("hasattr(stud2,age):",hasattr(stud2,'age'))
print("setattr(stud2,age,21):",setattr(stud2,'age',21))
stud2.detailstudent()
print("Age:",stud2.age)
print("delattr(stud2,age):",delattr(stud2,'age'))
stud3= student(65,'harry','chemistry')
print(stud3)
stud3.detailstudent()

#class attribute

class student:
    "common base class for all students"
    def __init__(self,rollno,name,course):
        self.rollno=rollno
        self.name=name
        self.course=course
    def displaystudent(self):
        print("Rollno:",self.rollno)
        print("Name:",self.name)
        print("course:",self.course)
stud1=student(10,"Jack","Ms")
print(stud1)
stud1.displaystudent()
print("student.___doc__:",student.__doc__)
print("student.__name__:",student.__name__)
print("student.__module__:",student.__module__)
print("student.__bases__:",student.__bases__)
print("student.__dict__:",student.__dict__)
