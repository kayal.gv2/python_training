#Data type converson
s = 'abcdef'
s1 = {1:'a', 2:'b', 3:'c' }
print("Conversion of", s , "to tuple is ",tuple(s))
print("Conversion of", s , "to list is ",list(s))
print("Conversion of", s , "to set is ",set(s))
print("Conversion of", s , "to frozenset is",frozenset(s))
print("Conversion of", s1 , "to dictionary is",dict(s1))
