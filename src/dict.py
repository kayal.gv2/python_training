A  = {"name": "nandhu ", "item": "choclate","order":2314 ,"count":44,}
print(type(A))

#Access
print(A["item"])
b = "item"
print(A[b])

#Delete
del A["count"]
print(A)

A.pop("order")
print(A)

A.popitem()

if "name" in A:
    print("true")

#if
if "dept" in A:
    print("true")
try:
    print(A["dept"])
except:
    print("  no keys found  in this  dictionary", A)


for key in A:
    print("keys",":",A[key])

B = {"jasmine": 50, "rose": 30, "lotus": 60, "hibiscus": 45}
C = {"dairy": "milk", "cookies": "butter", "choclate": "coco","candy": "sugar"}
for key in B.keys():
    print("keys",":",B[key])

#values finding
for key,value in C.items():
    print("key",":",value)
#Copy
print(B)
D = B.copy()
print(D)

#update
print(B)
B1 ={"lilly": 100}
print(B1)
B.update(B1)
print("updated flower items",B)

#nested_dict
E = {"sea": "lobstor", "river": "eal", "lake": "fish" , "pond": "crab"}
F ={"sea1":{"sea":"labstor","river":"eal"},"sea2":{"lake":"fish", "pond":"crab"}}
print(F)
