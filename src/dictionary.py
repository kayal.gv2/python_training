#dictionary
mee=dict(name='meera',dept='cs',rollno=1233)
meena=mee["name"]
print(mee)
print(meena)

#del chance
physics = { 'class' : 'first_year', 'name' : 'karolin' ,'rollno' : 1321 }
print(physics)
physics['name'] = 'cathy'
print(physics)

#dictionary creation
mystery = dict(name="miya",age=25,city="newyork")
print(mystery)
mirror=mystery["name"]#access
print(mystery)

#update
physics.update({'batch':'morning'})
print(physics)

#delete
del physics['rollno']
print(physics)

#for looping
for mee in physics.values():
    print(mee)
for mee in physics.items():
    print(mee)    
#nested
d1 = { 'dept': 'mechanic','team':'football' }
d2 = {'dept2': 'electronic','team': 'valleyball' }
d3 = {'dept':d1,'dept2':d2}
print(d3)

#if condition
if 'team' in d2:
    print('yes',d2,'is','present' )
