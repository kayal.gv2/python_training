#program to find the the sum of   N  natural number
n= int(input ("Enter the limit:"))
sum = 0
i = 1
while i<= n:
    sum = sum + i
    i = i + 1
print("sum of first",n,"natural numbers is ", sum)

# demo while with else
count = 1
while count <= 5:
    print("Hello Welcome")
    count =  count + 1
else:
    print("Exit")
print("End of Program")
        
