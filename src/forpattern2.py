#12. Use a for loop to print a triangle like the one below. Allow the user to specify how high the
#triangle should be.
#*
#**
#***
#****

for i in range(4):
    print("*"*(i+1))
