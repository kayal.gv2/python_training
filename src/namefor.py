

# name 100 times
for name in range(100):
    print("kayal:", name)


for i in range(10):
    print(i+1,'kayal')
#vertical and horizontal
for kayal in range(10):
    print('kayal'*10)

#a program that outputs 100 lines, numbered 1 to 100 ,  each
# with your name.

for i in range(100):
    print(i+1,  'kayal')
# write a program that prints out a list of the integers from 1 to 20 and the squres
def printSquarevalues():
    l = list()
    for i in range(1,20):
        l.append(i**2)
    print(l)
printSquarevalues()

# descending order 100 to 1 ,2 difference.

for i in range(100,1,-2):
    print(i,'',end='')
#  7. Write a program that uses exactly four for loops to print the sequence of letters below.

#AAAAAAAAAABBBBBBBCDCDCDCDEFFFFFFG
for i in range(10):
    print("A",end=' ')
for i in range(8):
    print("B",end=' ')
for i in range(4):
    print("CD",end=' ')
print("E",end=' ')
for i in range(6):
    print("F",end=' ')
print("G",end=' ')


#10. Use a for loop to print a box like the one below. Allow the user to specify how wide and how
#high the box should be. [Hint: print( ' * ' *10) prints ten asterisks.]
#*******************
#*******************
#*******************
#

#*******************
for i in range(4):
     print("*"*6)

#11. Use a for loop to print a box like the one below. Allow the user to specify how wide and how
#high the box should be.
#*******************
#*
#*
#*
#
#*******************
for i in range(1):
    print("*"*10)
for i in range(4):
    print("*")
for i in range(1):
    print("*"*10)

#12. Use a for loop to print a triangle like the one below. Allow the user to specify how high the
#triangle should be.
#*
#**
#***
#****

for i in range(4):
    print("*"*(i+1))
