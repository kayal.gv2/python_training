# Write a python program to check the validity of password input by users.

import re
p = input ("Input your password:")
while True :
    if (len(p)<6 or len(p)<16):
        print("Invalid password")
    elif not re.search("[a-z]", p):
         print("Invalid password")
         break

    elif not re.search("[o-9]", p):
         print("Invalid password")
         break

    elif not re.search("[A-Z]", p):
         print("Invalid password")
         break

    elif not re.search("[$#@]", p):
         print("Invalid password")
         break

    else:
         print("valid password")
         break
