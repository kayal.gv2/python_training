# Simple pyramid Pattern
#  python 3.x code to demonstrate star Pattern
# Function to demonstrate printing Pattern

    # outer loo to handle number of rows
    # n in this case
    n = int(input("Enter the step size:"))
    for i in range(1, n+1):
        # inner loop to handle of columns
        # values changing acc. to outer loop
        for j in range(1, i+1):
            print(i, end=' ')
            # printing stars
            print()
