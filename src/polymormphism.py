class Abc:
    def __init__(self,a,b):
        self.a = a
        self.b = b
    def __str__(self):
        return "Abc (%d, %d)" % (self.a, self.b)
    def __add__(self,other):
        return Abc(self.a + other.a , self.b + self.b)
a1 = Abc(4,8)
a2 = Abc(6,-2)
print(a1+a2)

#
class Mobile:
    def __init__(self,model,year):
        self.model = model
        self. year = year
    def info(self):
        print(f"I am a mobile. my model is {self.model}.I am {self.year} released")
    def make_sound(self):
        print("Hello moto")
class Mobile2:
    def __init__(self,model,year):
        self.model = model
        self.year = year
    def info(self):
        print(f"I am a mobile.  my model is {self.model}.I am {self.year} released")
    def make_sound(self):
        print("Hello Have Nice Day")
redmi = Mobile2("redmi note7",2017)
moto = Mobile("motog7",2015)
for mobile in(redmi,moto):
    mobile.info()
    mobile.make_sound()

#

from math import pi

class Shape:
    def __init__(self,name):
        self.name = name
    def area(self):
        pass
    def fact(self):
         return "I am two dimensional shape."
    def __str__(self):
        return self.name
class Square(Shape):
    def __init__(self,length):
        super().__init__("Square")
        self.length = length
    def area(self):
        return self.length**2
    def fact(self):
        return "Square have each angle equal to 90 degree"
class Circle(Shape):
    def __init__(self,radius):
        super().__init__("Circle")
        self.radius = radius
    def area(self):
        return pi*self.radius**2
a = Square(6)
b = Circle(7)
print(b)
print(b.fact())
print(a.fact())
print(b.area())
