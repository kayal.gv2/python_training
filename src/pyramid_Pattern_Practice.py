# star Pyramid pattern
rows = 5 # outer loop to handle of rows

for i in range(1, rows):
    # inner loop to handle of columns
    for j in range(1,i+1):
        # printing stars
        print("*", end=' ')
    print()
