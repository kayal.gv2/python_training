# dipilay fibonacci series using recursion.
def recursive_fibo(n):
    if n <= 1:
        return n
    else:
        return(recursive_fibo(n-1) + recursive_fibo(n-2))

n = int(input("terms"))
if n <= 0:
    print("Please enter a positive integer")
else:
    print("Fibonacci sequence:")
    for i in range(n):
        print(recursive_fibo(i))
                        
