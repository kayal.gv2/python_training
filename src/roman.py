#Write a python class to convert an integer to a normal.

class py_solution:
    def in_to_Roman(self,num):
        val = [1000, 900, 500, 400,
               100, 90, 50, 40,
               10, 9, 5, 4,1]
        syb = [ "M", "CM", "D", "CD",
                "C", "XC", "L", "XL",
                 "X", "IX", "V","IV","I"]
        roman_num = ' '
        num1=num
        i= 0
        while num > 0:
            for  i in range(num // val[i]):
                  roman_num += syb[i]
                  num -= val[i]
            i += 1
        print("Roman Number of ",num1,"is",roman_num)
n = int(input("Enter an integer:"))
obj = py_solution()
obj.in_to_Roman(n)
