# Python 3.x code to demonstrate star Pattern
# Function to demonstrate printing Pattern
def pypart2(n):

    # number of spaces
    k = 2*n - 2

    # outer loop to handle of rows
    for i in range(0, n):
         # inner loop to handle number spaces
         # values changing acc. to requirement
         for j in range(0, k):
             print(end=" ")
        # decrementing k after each loop
         k = k - 2

        # inner loop to handle number of columns
        # values changing acc. to outer loop
         for j in range(0, i+1 ):
            #printing stars
            print(" * ", end=" ")
        # ending line after each rows
         print("\r")
# Driver code
n = 5
pypart2(n)
