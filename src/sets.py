a = {2,3,4,5,6,7}
print(type(a))
b = {'alpha', 'beta','gamma'}
print(type(b))
c = {20,20.5,41,11}
print(type(c))
#set creating
D = set(("add", "even", "assending"))
print(D)
#add
a.add(9)
print(a)

#remove
b.remove('gamma')
print(b)

#clear
c.clear()
print(c)

#length
print(len(a))

#for
a = {2,3,4,5,6,7}
for x in a:
    print(x)
print(7 in a)
# max
print("Maximum value in:", 'a',"is",max(a))

#sum
g = {12,13,14,15,16,32,22}
print(sum(g))

#union
print( "a:",a )
print("g:",g)
f = a.union(g)
print("union:",f)

#discard
D.remove("assending")
print(D)

#update
a.update(D)
print(a)


#intersection

x = {"apple", "orange", "cherry", "berrry"}
y = {"srawberry", "watermelon", "apple","rossberry"}
z = x.intersection(y)
print("intersection: ",z)

#symmetric difference
x = {"apple", "orange", "cherry", "berrry"}
y = {"srawberry", "watermelon", "apple","rossberry"}
x.symmetric_difference_update(y)
print(x)

z = x.symmetric_difference(y)
print(z)

#issubset()
s = {1,2,3,4,5}
r = {1,2,3,4,5,6,7,8,12}
m = s.issubset(r)
print(m)

#issuperset()
k = r.issuperset(s)
print(k) 
