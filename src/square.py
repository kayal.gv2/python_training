# Four digit perfect with even digits
import math
for i in range(1000,10000):
    num = int(math.sqrt(i))
    if num*num == i:
        n = i
        while n!=0:
            n = n/10
            if i%2!=0:
                break
            else:
                print(i)
