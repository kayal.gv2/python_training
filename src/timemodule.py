
#The following example shows how many seconds have elapsed since the epooch.
import time
seconds = time.time()
print("Number of seconds since 12:00am, January 1,2000:",seconds)

# example for ctime() method
import time
print("The time is:", time.time())
later = time.time() + 30
print("30 secs from now:", time.ctime(later))
